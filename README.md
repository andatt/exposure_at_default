To run with netting set data given in test doc:

```python ead_calculator.py```

To pass alternative netting set data in a script:

``` 
from ead_calculator import ExposureAtDefault
   
 
ead = ExposureAtDefault(path_to_data="some/path/to/data")
```

To calculate EAD for netting set in data:

```
ead.calculate()
```
