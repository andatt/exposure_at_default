import json
import datetime
import math


class ExposureAtDefault(object):

    SUPERVISORY_FACTORS = {
        "ir": 0.005
        # obtained from BIS doc pg 23:
        # https://www.bis.org/publ/bcbs279.pdf
        # can add more here as needed

    }

    HEDGING_SETS = {
        "ir": "currency_code"
        # can add the other sets as required
        # ignoring sub hedging sets for now
    }

    def __init__(self, path_to_data=None, alpha=1.4, collateral=0):
        """
        :param path_to_data: string - path to json file containing netting sets of derivative data (for a single counter
        party)
        :param alpha: int - alpha given by BIS pdf
        :param collateral: int - collateral exchanged at start
        """

        # netting set - same counter party
        # asset class - by asset
        # hedging set - by currency

        self.asset_class = None

        if path_to_data:
            with open(path_to_data) as data:
                json_data = json.load(data)

                self.data = self.sort_data(json_data["data"])

        self.results = {}
        self.alpha = alpha
        self.collateral = collateral

    def sort_data(self, json_data):
        """
        Sorts data by currency and maturity (i.e. hedging sets and sub hedging sets)
        :param json_data: dict
        :return: dict
        """
        # assuming all data is for the same counter party

        hedging_sets = {}

        for data in json_data:

            if not self.asset_class:
                self.asset_class = data["asset_class"]

            sub_hedging_sets = {
                "more than 5": [],
                "1 to 5": [],
                "less than 1": []
            }
            basis = ""
            if data["payment_type"] == "floating":
                if data["receive_type"] == "floating":
                    # this is a basis transaction which has a separate hedging set to non basis transactions
                    basis = "_basis"

            if hedging_sets.get(data["currency_code"] + basis):
                hedging_sets[data["currency_code"]] = self.sort_into_sub_hedging_set(
                    data, hedging_sets[data["currency_code"] + basis]
                )
                continue

            hedging_sets[data["currency_code"] + basis] = self.sort_into_sub_hedging_set(data, sub_hedging_sets)

        return hedging_sets

    def sort_into_sub_hedging_set(self, trade, sub_hedging_sets):

        end_date = datetime.datetime.strptime(trade["end_date"], "%Y-%m-%dT%H:%M:%SZ")
        remaining_maturity = int((end_date - datetime.datetime.today()).days / 365)

        if remaining_maturity > 5:
            sub_hedging_sets["more than 5"].append(trade)
        elif 1 < remaining_maturity < 5:
            sub_hedging_sets["1 to 5"].append(trade)
        else:
            sub_hedging_sets["less than 1"].append(trade)

        return sub_hedging_sets

    def adjusted_notional(self, trade, residual_maturity, optional_start=None):
        """
        calculates adjusted notional
        :param json_data: dict
        :param residual_maturity: int year to maturity
        :param optional_start: int year in which option exercised
        :return: float
        """
        supervisory_factor = self.SUPERVISORY_FACTORS[trade["asset_class"]]

        # formula from BIS doc pdf pg 19:
        # https://www.bis.org/publ/bcbs279.pdf

        start = 0
        if optional_start:
            start = optional_start

        supervisory_duration = (
            math.exp(-0.05 * start) - math.exp(-0.05 * residual_maturity)
                               ) / 0.05
        print("EXP1", math.exp(-supervisory_factor * start))
        print("EXP2", math.exp(-supervisory_factor * residual_maturity))
        print("Supervisory duration", supervisory_duration)
        return trade["notional_amount"] * supervisory_duration

    def maturity_factor(self, residual_maturity):
        """
        Calculates maturity factor
        :param residual_maturity:
        :return: float maturity factor
        """
        return math.sqrt(min(residual_maturity, 1) / 1)

    def supervisory_delta(self, trade):
        """
        Calculates supervisory delta
        :param trade: dict
        :return: int supervisory delta
        """
        if trade["asset_class"] == "ir":

            # buy - floating
            # buy - fixed
            # sell - floating
            # sell - fixed

            # sell fixed, buy floating = short (as will gain if price falls)
            # sell floating, buy fixed = long (as will gain if price rises)
            return_val = 1

            if trade["receive_type"] == "floating" and trade["payment_type"] == "fixed":
                # long
                return return_val

            if trade["receive_type"] == "fixed" and trade["payment_type"] == "floating":
                # short
                return -return_val

            # will be more logic in here for other asset classes

            return return_val

    def replacement_cost_add_on(self, hedging_set, hedging_set_name):
        """
        Calculates replacement cost and add on for a single hedging set

        Kept as joint function for speed - to avoid iterating through data set twice

        would investigate how to split this function into multiple parts given more time.

        :param hedging_set: list of dicts Each dict is a trade in FIRE format
        :param hedging_set_name string
        :return: int exposure at default
        """
        replacement_costs = []

        sub_hedging_sets_effective_notional_totals = {}

        for sub_hedging_set in hedging_set:

            trades = hedging_set[sub_hedging_set]
            shs_effective_notionals = []

            for trade in trades:
                trade_effective_notional = self.effective_notional(trade)
                shs_effective_notionals.append(trade_effective_notional)
                replacement_costs.append(trade["mtm_dirty"])

            total_effective_notional_for_this_shs = sum(shs_effective_notionals)
            sub_hedging_sets_effective_notional_totals[sub_hedging_set] = total_effective_notional_for_this_shs

        # square total of each bucket
        less_than_1_year = sub_hedging_sets_effective_notional_totals["less than 1"] ** 2
        one_to_5_years = sub_hedging_sets_effective_notional_totals["1 to 5"] ** 2
        more_than_5 = sub_hedging_sets_effective_notional_totals["more than 5"] ** 2

        # aggregate buckets in required manner
        maturity_bucket_total_1 = less_than_1_year + one_to_5_years + more_than_5
        maturity_bucket_total_2 = 1.4 * less_than_1_year * one_to_5_years
        maturity_bucket_total_3 = 1.4 * one_to_5_years * more_than_5
        maturity_bucket_total_4 = 0.6 * less_than_1_year * more_than_5

        buckets = maturity_bucket_total_1 + maturity_bucket_total_2 + maturity_bucket_total_3 + maturity_bucket_total_4

        aggregated_effective_notional = math.sqrt(buckets)

        add_on = aggregated_effective_notional * self.supervisory_factor(hedging_set_name)

        print("Add on for {}".format(hedging_set_name), add_on)

        return max(replacement_costs), add_on

    def calculate(self):
        """
        Calculates ead for netting set
        :return: int exposure at default for netting set
        """
        add_ons = []
        replacement_costs = []
        for hedging_set_name, set_data in self.data.items():
            replacement_cost, add_on = self.get_hedging_set_data(set_data, hedging_set_name)
            add_ons.append(add_on)
            replacement_costs.append(replacement_cost)
        # print("EAD for: {} = {}".format(hedging_set_type, ead))
        # self.results[hedging_set_type] = ead

        return self.exposure_at_default(replacement_costs, add_ons)

    def supervisory_factor(self, hedging_set_name):
        """
        Calculates supervisory factor
        :param hedging_set_name string
        :return: float add on
        """

        supervisory_factor = self.SUPERVISORY_FACTORS[self.asset_class]

        if "basis" in hedging_set_name:
            # floating, floating means a basis transaction hedging set therefore
            # x by 50% per BIS doc para 184
            # think we need the rate here when basis trans (floating, floating) to know if long or short?
            # just assuming +ve for now
            supervisory_factor *= 0.5

        return supervisory_factor

    def effective_notional(self, trade):
        """
        Calculates effective notional
        :param trade: dict containing trade data in FIRE format
        :return: float effective notional
        """

        start_date = datetime.datetime.strptime(trade["start_date"], "%Y-%m-%dT%H:%M:%SZ")
        end_date = datetime.datetime.strptime(trade["end_date"], "%Y-%m-%dT%H:%M:%SZ")
        residual_maturity = int((end_date - start_date).days / 365)
        print("Reisdual Maturity:", residual_maturity)
        adjusted_notional = self.adjusted_notional(trade, residual_maturity)
        maturity_factor = self.maturity_factor(residual_maturity)
        supervisory_delta = self.supervisory_delta(trade)
        print("Notional", trade["notional_amount"])
        print("Adjusted Notional", adjusted_notional)
        print("Maturity Factor", maturity_factor)
        print("supervisory_delta", supervisory_delta)

        return adjusted_notional * maturity_factor * supervisory_delta

    def exposure_at_default(self, replacement_costs, add_ons):
        """
        Calculates exposure at default
        :param replacement_costs: list
        :param add_on: int add on
        :return: int exposure at default
        """

        rc = sum(replacement_costs)
        net_mtm = sum(replacement_costs)
        add_on = sum(add_ons)

        if rc < 0:
            # replacement cost is deemed to be 0 when mtm = -ve
            rc = 0

        # formula from BIS doc pdf pg 10:
        # https://www.bis.org/publ/bcbs279.pdf

        multiplier = min(
            [
                1,
                0.05 + ((1 - 0.05) * math.exp((net_mtm - self.collateral) / (2 * (1 - 0.05) * add_on)))
            ]
        )

        pfe = multiplier * add_on

        print("rc", rc)
        print("net mtm", net_mtm)
        print("multiplier", multiplier)
        print("replacement cost", rc)
        print("Total add on", add_on)

        ead = self.alpha * (rc + pfe)
        return int(ead)


if __name__ == "__main__":
    ead_loaded = ExposureAtDefault("data.json")
    all_results = ead_loaded.calculate()
    print("all results:", all_results)

    # for result in ead_loaded.yield_result():
    #     print("Single result:", result)

