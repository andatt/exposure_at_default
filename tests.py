import pytest
from ead_caclulator import ExposureAtDefault


@pytest.mark.parametrize(
    "data, expected",
    [
        (
            [
                {
                    "id": "some derivative",
                    "asset_class": "ir",
                    "mtm_dirty": 1000,
                    "notional_amount": 15000,
                    "start_date": "2018-01-10T00:00:00Z",
                    "end_date": "2019-01-17T00:00:00Z",
                    "receive_type": "fixed",
                    "payment_type": "floating",
                    "currency_code": "USD"
                },
            ],
            1502

        ),
        (
            [
                {
                    "id": "some derivative",
                    "asset_class": "ir",
                    "mtm_dirty": 1500,
                    "notional_amount": 15000,
                    "start_date": "2015-01-10T00:00:00Z",
                    "end_date": "2019-01-17T00:00:00Z",
                    "receive_type": "fixed",
                    "payment_type": "floating",
                    "currency_code": "USD"
                },
            ],
            2480
        ),
        (
            [
                {
                    "id": "some derivative",
                    "asset_class": "ir",
                    "mtm_dirty": -1500,
                    "notional_amount": 15000,
                    "start_date": "2018-01-10T00:00:00Z",
                    "end_date": "2019-01-17T00:00:00Z",
                    "receive_type": "fixed",
                    "payment_type": "floating",
                    "currency_code": "USD"
                },
            ],
            5
        ),
        (
            [
                {
                    "id": "some derivative 1",
                    "asset_class": "ir",
                    "mtm_dirty": -300,
                    "notional_amount": 8000,
                    "start_date": "2017-01-10T00:00:00Z",
                    "end_date": "2019-01-17T00:00:00Z",
                    "receive_type": "fixed",
                    "payment_type": "floating",
                    "currency_code": "USD"
                },
                {
                    "id": "some derivative 2",
                    "asset_class": "ir",
                    "mtm_dirty": 300,
                    "notional_amount": 5000,
                    "start_date": "2016-01-10T00:00:00Z",
                    "end_date": "2019-01-17T00:00:00Z",
                    "receive_type": "fixed",
                    "payment_type": "floating",
                    "currency_code": "USD"
                },
                {
                    "id": "some derivative 3",
                    "asset_class": "ir",
                    "mtm_dirty": 2300,
                    "notional_amount": 4000,
                    "start_date": "2018-01-10T00:00:00Z",
                    "end_date": "2019-01-17T00:00:00Z",
                    "receive_type": "fixed",
                    "payment_type": "floating",
                    "currency_code": "USD"
                },
            ],
            3451
        )
    ]

)
def test_exposure_at_default(data, expected):

    ead = ExposureAtDefault()
    ead.data = ead.sort_data(data)
    assert ead.calculate() == expected
